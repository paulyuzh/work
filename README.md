# Work

![cover image1](images/repocover1.png)

![cover image2](images/repocover2.png)

![cover image3](images/repocover3.png)

![cover image4](images/repocover4.png)

#### Work is the prototype of a lifestyle app written in Swift 4. Main features include weather information, note-taking tool, and calendar.  
  
  
The project is written in Swift 4.

Use https://appetize.io to view the app.

Procedure:  
1. Go to https://appetize.io  
2. At the top tab bar, select "Upload"  
3. Click "Select file", and upload IDK.zip  
4. Enter email and click "Generate"  