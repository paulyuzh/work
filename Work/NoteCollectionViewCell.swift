//
//  NoteCollectionViewCell.swift
//  Work
//
//  Created by Paul Tang on 2019-04-06.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var content: UITextView!
    
}
