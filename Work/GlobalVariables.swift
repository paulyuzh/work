//
//  GlobalVariables.swift
//  Work
//
//  Created by Paul Tang on 2019-04-06.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import Foundation
import UIKit
import  os.log

// -- QUOTES STRUCT
struct Quote {
    let quote: String
    let person: String
    
    init(quote: String, person: String) {
        self.quote = quote
        self.person = person
    }
}

var quote_list: [Quote] = []

// -- NOTES STRUCT
class Note: NSObject, NSCoding {
    var content: String
    var date: String
    var id: String
    
    init(content: String, date: String, id: String) {
        self.content = content
        self.date = date
        self.id = id
    }
    
    //MARK: NSCoding
    func encode(with Coder: NSCoder) {
        Coder.encode(content, forKey: PropertyKey.content)
        Coder.encode(date, forKey: PropertyKey.date)
        Coder.encode(id, forKey: PropertyKey.id)
    }
    
    required convenience init?(coder Decoder: NSCoder) {
        // The noteDe is required. If we cannot decode a name string, the initializer should fail.
        guard let content = Decoder.decodeObject(forKey: PropertyKey.content) as? String else {
            os_log("Unable to decode the content for a Note object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        guard let date = Decoder.decodeObject(forKey: PropertyKey.date) as? String else {
            os_log("Unable to decode the date for a Note object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        guard let id = Decoder.decodeObject(forKey: PropertyKey.id) as? String else {
            os_log("Unable to decode the id for a Note object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        // Must call designated initializer.
        self.init(content: content, date: date, id: id)
        
    }
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("notes")
}

struct PropertyKey {
    static let content = "content"
    static let date = "date"
    static let id = "id"
}

func loadNotes() -> [Note]?  {
    var noteData: [Note]? = nil
    do {
        let data = try Data(contentsOf: Note.ArchiveURL)
        noteData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [Note]
    } catch {
        print("caught")
    }
    return noteData
    // edited from:
    //NSKeyedUnarchiver.unarchiveObject(withFile: Note.ArchiveURL.path) as? [Note]
}

func saveNoteToLocal() {
    
    do {
        let data = try NSKeyedArchiver.archivedData(withRootObject: note_list, requiringSecureCoding: false)
        try data.write(to: Note.ArchiveURL)
        os_log("Note successfully saved.", log: OSLog.default, type: .debug)
    } catch {
        os_log("Failed to save note.", log: OSLog.default, type: .error)
    }
    // edited from:
//    let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(note_list, toFile: Note.ArchiveURL.path)
//    if isSuccessfulSave {
//        os_log("Note successfully saved.", log: OSLog.default, type: .debug)
//    } else {
//        os_log("Failed to save note.", log: OSLog.default, type: .error)
//    }
}

var note_list: [Note] = []

// -- WEATHERS STRUCT
struct Weather {
    let temperature: String
    let date: String
    let image: UIImage
    let weatherDescription: String
    
    init(temperature: String, date: String, image: UIImage, weatherDescription: String) {
        self.temperature = temperature
        self.date = date
        self.image = image
        self.weatherDescription = weatherDescription
    }
}

var weather_list: [Weather] = []

func currentTime() -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    let result = formatter.string(from: date)
    
    let calendar = Calendar.current
    let hour = calendar.component(.hour, from: date) - 7
    let minutes = calendar.component(.minute, from: date)
    let seconds = calendar.component(.second, from: date)
    
    let time = result + "-" + String(format: "%02d", hour) + "-" + String(format: "%02d", minutes) + "-" + String(format: "%02d", seconds)
    return time
}
