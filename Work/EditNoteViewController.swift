//
//  EditNoteViewController.swift
//  Work
//
//  Created by Paul Tang on 2019-04-13.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit
import LGButton

class EditNoteViewController: UIViewController {
    
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var noteContent: UITextView!
    @IBOutlet weak var doneButton: LGButton!
    @IBOutlet weak var editNoteView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    let picker = UIDatePicker()
    
    var selectedNote: Note? = nil
    var tempDate: String = ""
    
    let buttonDoneText: String = "Save"
    let buttonDeleteText: String = "Delete"
    
    var effect: UIVisualEffect! // used to store the initial effect of the blur view so that it can be used during transformation

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // creates date picker
        createDatePicker()
        
        // tapping done button
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        doneButton.addGestureRecognizer(longGesture)
        
        // tap outside note view to dismiss keyboard
        let dismissKeyboard = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        blurView.addGestureRecognizer(dismissKeyboard)
        
        effect = blurView.effect
        blurView.effect = nil
        
        // Add shadow to pop up view
        editNoteView.layer.masksToBounds = false
        editNoteView.layer.shadowColor = UIColor.black.cgColor
        editNoteView.layer.shadowOffset = CGSize(width: 1, height: 2)
        editNoteView.layer.shadowOpacity = 0.4
        editNoteView.layer.cornerRadius = 20
        
        // Load note
        noteContent.text = selectedNote?.content
        dateTextField.text = String(selectedNote?.date.prefix(16) ?? "")
        tempDate = selectedNote?.date ?? ""
        
        animateIn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        doneButton.titleString = self.buttonDoneText
    }
    
    func createDatePicker(){
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //done button for the toolbar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        //creating flexible space (to create room so that the next button is at the right)
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let clear = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelPressed))
        toolbar.setItems([done, flexibleSpace, clear], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = picker
        
        //format picker for date
        picker.datePickerMode = .dateAndTime
    }
    
    @objc func cancelPressed(){
        dateTextField.text = ""
        tempDate = ""
        view.endEditing(true)
    }
    
    @objc func donePressed(){
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MMM d, hh:mm a"
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        // https://nsdateformatter.com/ is a great source to check dateFormat
        
        dateTextField.text = dateFormatter1.string(from: picker.date)
        // having the 2 formats stored in tempDate, use sections of the string for different formats
        tempDate = dateFormatter1.string(from: picker.date) + " - " + dateFormatter2.string(from: picker.date)
        view.endEditing(true)
        print(tempDate)
    }
    
    @objc func dismissKeyboard(_ sender: UIGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func longTap(_ sender: UIGestureRecognizer){
        if sender.state == .ended {
            // When touch is removed
        }
        else if sender.state == .began {
            // when touch begins
            if doneButton.titleString == buttonDoneText {
                UIView.animate(withDuration: 0.3,
                               animations: {
                                self.doneButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                },
                               completion: { _ in
                                UIView.animate(withDuration: 0.3) {
                                    self.doneButton.transform = CGAffineTransform.identity
                                    self.doneButton.titleString = self.buttonDeleteText
                                    
                                    self.doneButton.gradientStartColor = UIColor(red: 255/255, green: 51/255, blue: 51/255, alpha: 1.0)
                                    self.doneButton.gradientEndColor = UIColor.white
                                    // the .gradientHorizontal is added for LGButton code does not
                                    // reset gradient and this causes the setGradient func to not
                                    // be called; however, .gradientHorizontal resets gradient
                                    self.doneButton.gradientHorizontal = false
                                }
                })
            } else if doneButton.titleString == buttonDeleteText {
                UIView.animate(withDuration: 0.3,
                               animations: {
                                self.doneButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                },
                               completion: { _ in
                                UIView.animate(withDuration: 0.3) {
                                    self.doneButton.transform = CGAffineTransform.identity
                                    self.doneButton.titleString = self.buttonDoneText
                                    
                                    self.doneButton.gradientStartColor = UIColor(red: 94/255, green: 177/255, blue: 255/255, alpha: 1.0)
                                    self.doneButton.gradientEndColor = UIColor.white
                                    self.doneButton.gradientHorizontal = false
                                }
                })
            }
        }
    }
    
    @IBAction func dismissButton(_ sender: LGButton) {
        if doneButton.titleString == buttonDoneText {
            saveNote()
        } else if doneButton.titleString == buttonDeleteText {
            deleteNote()
        }
        saveNoteToLocal()
        animateOut()
    }
    
    @IBAction func dismissWithoutSavinf(_ sender: Any) {
        animateOut()
    }
    
    func saveNote() {
        if !noteContent.text.trimmingCharacters(in: .whitespaces).isEmpty {
            // save content
            selectedNote?.content = noteContent.text
            // save date
            selectedNote?.date = tempDate
            
        } else {
            // if there is nothing written, delete note instead
            deleteNote()
        }
    }
    
    func deleteNote() {
        guard let id = selectedNote?.id else {
            return
        }
        
        for note in note_list {
            if id == note.id {
                note_list = note_list.filter({ (note) -> Bool in
                    return !(note.id == id)
                })
            }
        }
    }
    
    
    func animateIn() {
        
        editNoteView.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
        editNoteView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.blurView.effect = self.effect
            self.editNoteView.alpha = 1
            self.editNoteView.transform = CGAffineTransform.identity
        }
    }

    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self.editNoteView.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            self.editNoteView.alpha = 0
            self.blurView.effect = nil
        }) { (success) in
            self.view.removeFromSuperview()
            // reload noteCollectionView from FirstVC
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadCalendar"), object: nil)
        }
    }
    
}
