//
//  WeatherCollectionViewCell.swift
//  Work
//
//  Created by Paul Tang on 2019-04-06.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var image: UIImageView!
    
}
