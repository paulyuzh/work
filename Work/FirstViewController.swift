//
//  FirstViewController.swift
//  Work
//
//  Created by Paul Tang on 2019-04-02.
//  Copyright © 2019 Paul Tang. All rights reserved.
//

import UIKit
import Pastel
import Floaty

class FirstViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    @IBOutlet weak var noteCollectionView: UICollectionView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var dateActual: UILabel!
    @IBOutlet weak var weatherView: UIView!
    
    let edited_weather_list = Array(weather_list.dropFirst())
    
    let floaty = Floaty()
    
    private func addBackgroundGradient() {
        let collectionViewBackgroundView = UIView()
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = view.frame.size
        // Start and end for left to right gradient
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [UIColor(red: 66/255, green: 134/255, blue: 244/255, alpha: 1.0).cgColor,
                                UIColor(red: 0/255, green: 70/255, blue: 183/255, alpha: 1.0).cgColor]
        noteCollectionView.backgroundView = collectionViewBackgroundView
        noteCollectionView.backgroundView?.layer.addSublayer(gradientLayer)
    }
    
    @objc func loadList(notification: NSNotification){
        self.noteCollectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add selector
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name:NSNotification.Name(rawValue: "load"), object: nil)

        
        weatherCollectionView.delegate = self
        weatherCollectionView.dataSource = self
        
        noteCollectionView.delegate = self
        noteCollectionView.dataSource = self
        
        //load today's weather info
        date.text = weather_list[0].date
        temp.text = weather_list[0].temperature
        image.image = weather_list[0].image
        weatherDescription.text = weather_list[0].weatherDescription
        dateActual.text = "Feb,26,2019"
        
        let pastelView = PastelView(frame: view.bounds)
        
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 1.0
        
        // Custom Color
        pastelView.setColors([UIColor(red: 66/255, green: 134/255, blue: 244/255, alpha: 1.0),
                              UIColor(red: 0/255, green: 70/255, blue: 183/255, alpha: 1.0),])

        
        pastelView.startAnimation()
        pastelView.tag = 100
        weatherView.insertSubview(pastelView, at: 0)
        
        addBackgroundGradient()
        
        // Add Floaty button
        floaty.addItem("Add New Note", icon: UIImage(named: "note")!, handler: { item in
            let newNote: Note = Note(content: "", date: "", id: currentTime())
            note_list.append(newNote)
            // initial popup view
            if let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditNoteVC") as? EditNoteViewController {
                popUpVC.selectedNote = newNote
                self.addChild(popUpVC)
                popUpVC.view.frame = self.view.frame
                self.view.addSubview(popUpVC.view)
                popUpVC.didMove(toParent: self)
            }
            self.floaty.close()
        })
        self.view.addSubview(floaty)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // remove the already existing pastelView first (tag 100)
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }
        
        // Add animated background to weatherView again
        let pastelView = PastelView(frame: view.bounds)
        
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([UIColor(red: 66/255, green: 134/255, blue: 244/255, alpha: 1.0),
                              UIColor(red: 0/255, green: 70/255, blue: 183/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 203/255, blue: 244/255, alpha: 1.0),
                              UIColor(red: 86/255, green: 134/255, blue: 255/255, alpha: 1.0)])
        
        
        pastelView.startAnimation()
        pastelView.tag = 100
        weatherView.insertSubview(pastelView, at: 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.noteCollectionView {
            let note = note_list[indexPath.row]
            // initial popup view
            if let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditNoteVC") as? EditNoteViewController {
                popUpVC.selectedNote = note
                self.addChild(popUpVC)
                popUpVC.view.frame = self.view.frame
                self.view.addSubview(popUpVC.view)
                popUpVC.didMove(toParent: self)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.noteCollectionView {
            return note_list.count
        }
        
        if collectionView == self.weatherCollectionView {
            return edited_weather_list.count
        }
        
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.noteCollectionView {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteReuseAtHome", for: indexPath) as? NoteCollectionViewCell {
                
                // Corner radius and shadow for cell
                cell.layer.cornerRadius = 12.0
                cell.layer.borderWidth = 3.0
                cell.layer.borderColor = UIColor.darkGray.cgColor
                cell.layer.masksToBounds = true
                cell.layer.shadowColor = UIColor.lightGray.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 12.0
                cell.layer.shadowOpacity = 0.8
                cell.layer.masksToBounds = false
                cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
                
                let note = note_list[indexPath.row]
                
                cell.date.text = String(note.date.prefix(16))
                cell.content.text = note.content
                
                return cell
                
            } else {
                return UICollectionViewCell()
            }
        }
        
        if collectionView == self.weatherCollectionView {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherReuseAtHome", for: indexPath) as? WeatherCollectionViewCell {
                
                let weather = edited_weather_list[indexPath.row]
                
                
                cell.date.text = weather.date
                cell.temperature.text = weather.temperature
                cell.image.image = weather.image
                
                return cell
            } else {
                return UICollectionViewCell()
            }
        }
            
        else {
            return UICollectionViewCell()
        }
    }
    
    
    
}
