//  CalendarViewController.swift
//  calendarTest
//
//  Created by Guest User on 2018-03-07.
//  Copyright © 2018 Guest User. All rights reserved.
//

import UIKit

class CalendarVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var displayText: UILabel!
    
    @IBOutlet weak var calendarTable: UITableView!
    
    @IBOutlet weak var calendarView: UICollectionView!
    
    var refreshControl = UIRefreshControl()
    
    // 37 dates
    var selectedMonth = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]
    
    var monthIndex: Int = -1
    var yearIndex: Int = -1
    var finalMonthIndex: String = ""
    var result: String = ""
    var datePicked: String = ""
    var displayedNotes = [Note]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarTable.delegate = self
        calendarTable.dataSource = self
        
        calendarView.delegate  = self
        calendarView.dataSource = self
        
        let nav = self.navigationController?.navigationBar
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 120/225.0, green: 143/225.0, blue: 170/225.0, alpha: 1)]
        
        let itemSize = UIScreen.main.bounds.width/7
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        calendarView.collectionViewLayout = layout
        
        // add selector
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCollectionView), name:NSNotification.Name(rawValue: "loadCalendar"), object: nil)
        
        // Pull to update
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender: )), for: UIControl.Event.valueChanged)
        calendarView.addSubview(refreshControl)

        
        // get the current date
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        result = formatter.string(from: today)
        
        // year value to int
        let indexOfYear = result.index(result.endIndex, offsetBy: -6)
        yearIndex = Int(result[..<indexOfYear]) ?? -1  //yyyy
        
        // month value to int
        let indexofMonth1 = result.index(result.startIndex, offsetBy: 5)
        let indexofMonth2 = result.index(result.endIndex, offsetBy: -3)
        let range = indexofMonth1..<indexofMonth2
        monthIndex = Int(result[range]) ?? -1
        finalMonthIndex = String(format: "%02d", monthIndex)
    }
    
    @objc func reloadCollectionView(notification: NSNotification){
        self.calendarView.reloadData()
    }
    
    @objc func refresh(sender: AnyObject) {
        displayedNotes.removeAll()
        calendarView.reloadData()
        calendarTable.reloadData()
        refreshControl.endRefreshing()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        displayedNotes.removeAll()
        calendarView.reloadData()
        calendarTable.reloadData()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func nextMonth(_ sender: Any) {
        if monthIndex < 12 {
            monthIndex += 1
        } else {
            yearIndex += 1
            monthIndex = 1
        }
        finalMonthIndex = String(format: "%02d", monthIndex)
        
        for i in selectedMonth.indices {
            selectedMonth[i] = ""
        }
        calendarView.reloadData()
        
    }
    
    @IBAction func lastMonth(_ sender: Any) {
        if monthIndex > 1 {
            monthIndex -= 1
        } else {
            yearIndex -= 1
            monthIndex = 12
        }
        finalMonthIndex = String(format: "%02d", monthIndex)
        
        for i in selectedMonth.indices {
            selectedMonth[i] = ""
        }
        calendarView.reloadData()
        
    }
    
    func getDayOfWeekFunc(today:String)->Int? {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: today) {
            let myCalendar = Calendar(identifier: .gregorian)
            let myComponents = myCalendar.component(.weekday, from: todayDate)
            let weekDay = myComponents
            return weekDay
        }
        return nil
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        displayedNotes = []
        
        let date = collectionView.cellForItem(at: indexPath)
        
        if selectedMonth[indexPath.row] != "" {
            let checkDate =  String(format: "%02d", Int(selectedMonth[indexPath.row]) ?? -1)
            date?.backgroundColor = UIColor.yellow
            
            // this is the actual date picked
            let indexOfDatePicked = result.index(result.endIndex, offsetBy: -2)
            datePicked = result[..<indexOfDatePicked] + checkDate
            
            for note in note_list {                
                if datePicked == String(note.date.suffix(10)) {
                    displayedNotes += [note]
                }
            }
            
        }
        calendarTable.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let date = collectionView.cellForItem(at: indexPath)
        date?.backgroundColor = UIColor.clear
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedMonth.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell: CalendarCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarCollection", for: indexPath) as? CalendarCVCell {
            
            cell.backgroundColor = UIColor.clear
            cell.image.isHidden = true
            cell.image.layer.cornerRadius = 4
            cell.image.layer.masksToBounds = true
            cell.image2.isHidden = true
            cell.image2.layer.cornerRadius = 4
            cell.image2.layer.masksToBounds = true
            
            
            // update result
            result = String(yearIndex) + "-" + finalMonthIndex + "-01"
            
            // displaying year & month
            let displayTextIndex = result.index(result.endIndex, offsetBy: -3)
            displayText.text = String(result[..<displayTextIndex])
            
            
            guard var weekday = getDayOfWeekFunc(today: result) else{
                return UICollectionViewCell()
            }
            
            if weekday == 7 {
                weekday = 1
            }
            
            // get the # of days in a given month & year
            let dateComponents = DateComponents(year: yearIndex
                , month: monthIndex)
            let calendar = Calendar.current
            if let date = calendar.date(from: dateComponents) {
                
                let range = calendar.range(of: .day, in: .month, for: date)
                if let numDays: Int = range?.count {
                    
                    //add dates to selectedMonth array
                    var day: Int = 1
                    while day <= numDays {
                        selectedMonth[weekday - 1] = String(day)
                        weekday += 1
                        day += 1
                    }
                }
            }
            
            let dates = selectedMonth[indexPath.row]
            cell.number?.text = dates
            
            // Highlight all the events
            for checkCell in [cell] as [CalendarCVCell] {
                for note in note_list {
                    // it is important to have the check !="" for "" cells can't be Int
                    if checkCell.number.text != "" {
                        if displayText.text! + "-" + String(format: "%02d", Int(checkCell.number.text!)!) == String(note.date.suffix(10)) {
                            checkCell.image.isHidden = false
                        }
                    }
                }
            }
            
            return cell
        
        } else {
            assertionFailure("Unable to dequeue cell")
            return UICollectionViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = displayedNotes[indexPath.row]
        
        // initial popup view
        if let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditNoteVC") as? EditNoteViewController {
            popUpVC.selectedNote = note
            self.addChild(popUpVC)
            popUpVC.view.frame = self.view.frame
            self.view.addSubview(popUpVC.view)
            popUpVC.didMove(toParent: self)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayedNotes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: CalendarTVCell = tableView.dequeueReusableCell(withIdentifier: "CalendarTable", for: indexPath) as? CalendarTVCell {
            
            let notesToDisplay = displayedNotes[indexPath.row]
            cell.noteContent.text = notesToDisplay.content
            
            return cell
            
        } else {
            assertionFailure("Unable to dequeue cell")
            return UITableViewCell()
        }
        
    }
    
     // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     }
    
    
}


